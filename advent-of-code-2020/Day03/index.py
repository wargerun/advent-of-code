TREE_SYMBOL = '#'


def get_lines_from_file():
    with open('input.txt', 'r') as file:
        return file.read().splitlines()


def isTree(pattern: str, column: int) -> bool:
    column = column % len(pattern)
    symbol = pattern[column]
    return symbol == TREE_SYMBOL


def part_1():
    RIGHT = 3
    DOWN = 1
    lines = get_lines_from_file()

    column = 0
    row = 0
    count_trees = 0

    while row < len(lines):
        pattern = lines[row]

        if isTree(pattern, column):
            count_trees += 1

        column += RIGHT
        row += DOWN

    print (F'RESULT {count_trees}')


def part_2():
    slopes = [
        (1, 1),
        (3, 1),
        (5, 1),
        (7, 1),
        (1, 2),
    ]

    lines = get_lines_from_file()
    cache_tree_counters = []

    for right, down in slopes:
        column = 0
        row = 0
        count_trees = 0

        while row < len(lines):
            pattern = lines[row]

            if isTree(pattern, column):
                count_trees += 1

            column += right
            row += down

        cache_tree_counters.append(count_trees)
        print(F"Right={right}, down={down}, trees={count_trees}")

    multiply_trees = 1
    for count in cache_tree_counters:
        multiply_trees *= count

    print (F'RESULT {multiply_trees}')


def main():
    part_2()


if __name__ == "__main__":
    main()
