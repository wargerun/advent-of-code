﻿using System;
using System.Collections.Generic;

namespace AdventOfCoded2020
{
    public static class CommonHelper
    {
        public static IEnumerable<T> Foreach<T>(this IEnumerable<T> collection, Action<T> action)
        {
            return collection.Foreach((item, index) => action(item));
        }

        public static IEnumerable<T> Foreach<T>(this IEnumerable<T> collection, Action<T, int> action)
        {
            int index = 0;

            foreach (T item in collection)
            {
                action(item, index);
                index++;
            }

            return collection;
        }
    }
}
