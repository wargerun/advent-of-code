﻿using System.IO;

namespace AdventOfCoded2020
{
    public abstract class DayBase : IDay
    {
        public abstract int Day { get; }
        public abstract void Execute();

        protected string[] GetInput(string fileName = "input.txt")
        {
            string filePath = $"../../../Day{Day}/{fileName}";
            return File.ReadAllLines(filePath);
        }
    }
}
