﻿using AdventOfCode2020.Day1;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace AdventOfCoded2020.Day01
{
    public class BrutForceResolutionOfDay
    {
        private const int RESULT = 2020;

        public void Get_Part_1_v1()
        {
            int[] numbers = (int[])new InputDataResolver().GetData();
            Stopwatch sw = Stopwatch.StartNew();

            foreach (var currentNumber in numbers)
            {
                foreach (int roundNumber in numbers)
                {
                    if (currentNumber + roundNumber == RESULT)
                    {
                        Console.WriteLine($"{currentNumber} * {roundNumber} = {currentNumber * roundNumber}");
                    }
                }
            }

            sw.Stop();

            File.AppendAllLines("BrutForceResolutionOfDay.txt", new List<string>() { sw.ElapsedMilliseconds.ToString() });
            Console.WriteLine($"Time: {sw.Elapsed.TotalMilliseconds}");
        }

        public void Get_Part_2_v1()
        {
            int[] numbers = (int[])new InputDataResolver().GetData();

            foreach (var currentNumber in numbers)
            {
                foreach (int firstRoundNumber in numbers)
                {
                    foreach (int secondRoundNumber in numbers)
                    {
                        if (currentNumber + firstRoundNumber + secondRoundNumber == RESULT)
                        {
                            Console.WriteLine($"Find = {currentNumber * firstRoundNumber * secondRoundNumber}");
                        }
                    }
                }
            }
        }
    }
}
