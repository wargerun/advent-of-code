﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace AdventOfCode2020.Day01
{
    public class OptimizeResolution
    {
        private const int RESULT = 2020;

        public void Get()
        {
            int[] numbers = (int[])new InputDataResolver().GetData();
            Stopwatch sw = Stopwatch.StartNew();

            foreach (var currentNumber in numbers)
            {
                if (numbers.Contains(RESULT - currentNumber))
                {
                    Console.WriteLine($"Find: {(RESULT - currentNumber) * currentNumber}");
                }
            }

            sw.Stop();

            File.AppendAllLines("OptimizeResolution.txt", new List<string>() { sw.Elapsed.TotalMilliseconds.ToString() });
            Console.WriteLine($"Time: {sw.Elapsed.TotalMilliseconds}");
        }
    }
}
