﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCoded2020.Day09
{
    public class Day9 : DayBase
    {
        private const int PREVIOUS_NUMBERS = 25;
        public override int Day => 9;

        public override void Execute()
        {
            long[] numbers = GetInput().Select(long.Parse).ToArray();

            long notValidValue = getNotValidValue(numbers);
            Console.WriteLine($"Part 1 is {notValidValue }");

            List<long> listNumbers = GetListNumbersSummary(numbers.ToList(), notValidValue);
            Console.WriteLine($"Part 2 is {listNumbers.Min() + listNumbers.Max()}");
        }

        private static List<long> GetListNumbersSummary(List<long> numbers, long needValue)
        {
            List<long> numbersResult = new List<long>();
            long sumNumber = 0;

            while (sumNumber <= needValue)
            {
                foreach (long number in numbers)
                {
                    numbersResult.Add(number);
                    sumNumber += number;

                    if (sumNumber == needValue)
                    {
                        return numbersResult;
                    }
                    else if (sumNumber > needValue)
                    {
                        sumNumber = 0;
                        numbersResult.Clear();
                        break;
                    }
                }

                numbers.RemoveAt(index: 0);
            }

            throw new InvalidOperationException();
        }

        private long getNotValidValue(long[] numbers)
        {
            for (int currentIndex = PREVIOUS_NUMBERS; currentIndex < numbers.Length; currentIndex++)
            {
                long[] prev5Numbers = getPrev5Numbers(numbers, currentIndex);
                long currentNumber = numbers[currentIndex];

                if (!isValidValue(prev5Numbers, currentNumber))
                {
                    return currentNumber;
                }
            }

            return -1;
        }

        private bool isValidValue(long[] prev5Numbers, long currentNumber)
        {
            foreach (long number in prev5Numbers)
            {
                foreach (long otherNumber in prev5Numbers)
                {
                    if (number + otherNumber == currentNumber)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private long[] getPrev5Numbers(long[] numbers, int currentIndex)
        {
            int subtract = currentIndex - PREVIOUS_NUMBERS;

            if (subtract < 0 || subtract > numbers.Length - 1)
            {
                throw new InvalidOperationException();
            }

            return Enumerable.Range(subtract, PREVIOUS_NUMBERS).Select(i => numbers[i]).ToArray();
        }
    }
}
