import re as regxp
from typing import List


def get_lines_from_file():
    with open('input.txt', 'r') as file:
        return file.read().splitlines()


class Passport:
    def __init__(self):
        self.FIELDS_REQUIRED = [ 'byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid' ]
        self.PROPERTIES = {} 


    def isRequiredFilesExists(self) -> bool:
        for field in self.FIELDS_REQUIRED:
            if field not in self.PROPERTIES.keys():
                return False

        return True


    def isValidBirthYear(self, value : str) -> bool :
        """
            byr (Birth Year) - four digits; at least 1920 and at most 2002.
        """
        return 1920 <= int(value) <= 2002


    def isValidIssueYear(self, value : str) -> bool:
        """
            iyr (Issue Year) - four digits; at least 2010 and at most 2020.
        """
        return 2010 <= int(value) <= 2020


    def isValidExpirationYear(self, value : str) -> bool:
        """
            eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
        """
        return 2020 <= int(value) <= 2030


    def isValidHeight(self, value : str) -> bool:
        """
            hgt (Height) - a number followed by either cm or in:
                If cm, the number must be at least 150 and at most 193.
                If in, the number must be at least 59 and at most 76. 
        """
        if 'cm' in value:
            value = value[:-2]
            return 150 <= int(value) <= 193
        elif 'in' in value:
            value = value[:-2]
            return 59 <= int(value) <= 76
        else:
            return False


    def isValidHairColor(self, value : str) -> bool:
        """
            hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
        """
        return regxp.match('^#(?:[0-9]|[a-f])+$', value)


    def isValidEyeColor(self, value : str) -> bool:
        """
            ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
        """
        return regxp.match('^amb|blu|brn|gry|grn|hzl|oth$', value)


    def isValidPassportId(self, value : str) -> bool:
        """
            pid (Passport ID) - a nine-digit number, including leading zeroes.
        """
        return regxp.match('^(?:\d{9})$', value)


    def isValid(self) -> bool:
        if not self.isRequiredFilesExists():
            return False

        KEY_MAP_FUNC = {
            'byr': self.isValidBirthYear,
            'iyr': self.isValidIssueYear,
            'eyr': self.isValidExpirationYear,
            'hgt': self.isValidHeight,
            'hcl': self.isValidHairColor,
            'ecl': self.isValidEyeColor,
            'pid': self.isValidPassportId,
        }

        for key in self.PROPERTIES:
            value = self.PROPERTIES[key]
            function = KEY_MAP_FUNC.get(key, None)

            if function and not function(value):
                return False

        return True


def get_passports_from_file() -> List['Passport']:
    lines = get_lines_from_file()
    passports = []
    current_passport = Passport()

    for line in lines:
        if not line:
            passports.append(current_passport)
            current_passport = Passport()
            continue

        pairs = line.split(' ')
        for pair in pairs:
            key, value = pair.split(':')
            current_passport.PROPERTIES[key] = value
    
    passports.append(current_passport)

    print (f'passports.length {len(passports)}')
    return passports


def isValid(passport : Passport) -> bool:
    if not passport.isValid():
        return False

    for key in passport.PROPERTIES:
        value = passport.PROPERTIES[key]
        function = KEY_MAP_FUNC.get(key, None)

        if function and not function(value):
            return False

    return True

def main():
    passports = get_passports_from_file()
    valid_passports = 0
    Valid_isRequiredFilesExists = 0
    
    for passport in passports:
        if passport.isRequiredFilesExists():
            Valid_isRequiredFilesExists += 1

        if passport.isValid(): 
            valid_passports += 1

    print (f'valid_passports {valid_passports} - {Valid_isRequiredFilesExists}')


if __name__ == "__main__":
    main()
