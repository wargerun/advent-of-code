﻿namespace AdventOfCoded2020.День11
{
    public partial class День11
    {
        struct Седло
        {
            public char Место { get; }

            public Седло(char seat)
            {
                Место = seat;
            }

            public static Седло СвободноСедло() => new Седло('L');
            
            public static Седло ЗанятоеСедло() => new Седло('#');

            public string ВСТРОКУ => new string(Место, 1);

            public bool ЭтоПол => Место == '.';
            public bool ЭтоЗанято => Место == '#';
            public bool ЭтоСвободно => Место == 'L';
        }
    }
}
