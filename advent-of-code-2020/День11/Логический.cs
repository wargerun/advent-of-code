﻿namespace AdventOfCoded2020.День11
{
    readonly struct Логический
    {
        const bool НЕПРАВДА = false;
        const bool ПРАВДА = НЕПРАВДА == НЕПРАВДА;

        private readonly bool _булеан;

        public Логический(bool булеан) => _булеан = булеан;

        public static readonly Логический Правда = new Логический(ПРАВДА);
        public static readonly Логический Неправда = new Логический(НЕПРАВДА);

        public static implicit operator bool(Логический булеан) => булеан._булеан;
        public static explicit operator Логический(bool булеан) => new Логический(булеан);
    }
}
