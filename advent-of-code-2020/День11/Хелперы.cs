﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCoded2020.День11
{
    public static class Хелперы
    {
        public static int Количество<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate) => source.Count(predicate);
        public static bool Любое<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate) => source.Any(predicate);

        public static TSource[] ВМасив<TSource>(this IEnumerable<TSource> source) => source.ToArray();
        public static IEnumerable<TResult> Селект<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult> selector) => source.Select(selector);
    }
}
