﻿using System;
using System.Collections.Generic;

namespace AdventOfCoded2020.Day08
{
    public class HandheAccumalator
    {
        public int Accumulator { get; private set; }

        public void AccumalatorCalculate(Operation[] operations)
        {
            int currentPosition = 0;
            Accumulator = 0;
            List<int> historyOfPosition = new List<int>();

            while (true)
            {
                if (currentPosition < 0 || currentPosition > operations.Length - 1)
                {
                    throw new InvalidOperationException();
                }

                if (historyOfPosition.Contains(currentPosition))
                {
                    break;
                }

                historyOfPosition.Add(currentPosition);
                Operation operation = operations[currentPosition];

                switch (operation.Command)
                {
                    case Command.Nop:
                        currentPosition++;
                        break;
                    case Command.Acc:
                        currentPosition++;
                        Accumulator += operation.Pointer;
                        break;
                    case Command.Jmp:
                        currentPosition += operation.Pointer;
                        break;
                }
            }
        }
    }
}
