﻿using System;

namespace AdventOfCoded2020.Day08
{
    public class Operation
    {
        public Command Command { get; private set; }
        public int Pointer { get; private set; }

        public Operation(Command command, int pointer)
        {
            Command = command;
            Pointer = pointer;
        }

        public static Operation GetOperation(string line)
        {
            string[] words = line.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            AssertWords(words);

            if (!Enum.TryParse<Command>(words[0], true, out Command command))
            {
                throw new InvalidOperationException("Invalid Command");
            }

            return new Operation(command, int.Parse(words[1]));
        }

        private static void AssertWords(string[] words)
        {
            if (words is null)
            {
                throw new ArgumentNullException(nameof(words));
            }

            if (words.Length > 2)
            {
                throw new ArgumentOutOfRangeException(nameof(words));
            }
        }

        public override string ToString()
        {
            return $"{Command} - {Pointer}";
        }
    }
}
