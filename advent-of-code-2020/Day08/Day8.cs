﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCoded2020.Day08
{
    public enum Command
    {
        Nop,
        Acc,
        Jmp,
    }

    public class Day8 : DayBase
    {
        public override int Day => 8;

        public override void Execute()
        {
            Operation[] operations = GetInput().Select(Operation.GetOperation).ToArray();
            HandheAccumalator handleAccumalator = new HandheAccumalator();

            handleAccumalator.AccumalatorCalculate(operations);
            Console.WriteLine($"Part 1: accumulator={handleAccumalator.Accumulator}");
            
            foreach (Operation[] newCperations in OperationSwapper(operations))
            {
                try
                {
                    handleAccumalator.AccumalatorCalculate(newCperations);
                }
                catch (InvalidOperationException)
                {
                    Console.WriteLine($"Part 2: accumulator={handleAccumalator.Accumulator}");
                }
            }

            Console.WriteLine($"OK, I'M DONE, MAZAFUCKER !!");
        }

        private IEnumerable<Operation[]> OperationSwapper(Operation[] operations)
        {
            for (int i = 0; i < operations.Length; i++)
            {
                Operation[] copyOperation = operations.ToArray();
                Operation operation = copyOperation[i];

                switch (operation.Command)
                {
                    case Command.Nop:
                        copyOperation[i] = new Operation(Command.Jmp, operation.Pointer);
                        yield return copyOperation;
                        break;
                    case Command.Jmp:
                        copyOperation[i] = new Operation(Command.Nop, operation.Pointer);
                        yield return copyOperation;
                        break;
                }

            }
        }
    }
}
