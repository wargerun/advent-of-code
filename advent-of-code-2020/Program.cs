﻿namespace AdventOfCoded2020
{
    class Program
    {
        static void Main(string[] args)
        {
            new День11.День11().Выполнить();
            System.Console.ReadKey();
        }

        private static void DayExecutor(IDay day) => day.Execute();
    }
}
