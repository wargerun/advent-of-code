import re as regex

def get_passwords():
    with open('input.txt', 'r') as file:
        return file.read().splitlines()

def parse_line(line : str) -> (int, int, str, str):
    match = regex.search('(\d*)-(\d*) (\w): (\w*)', line)

    startWith = int(match.group(1))
    endWith = int(match.group(2))
    letter = match.group(3)
    word = match.group(4)

    return startWith, endWith, letter, word

def part_1():
    validLines = 0

    for line in get_passwords():
        startWith, endWith, letter, word = parse_line(line)

        if count >= startWith and count <= endWith:
            validLines += 1

    print (F'part 1 validLines = {validLines}')

def part_2():
    validLines = 0

    for line in get_passwords():
        startWith, endWith, letter, word = parse_line(line)

        if word[startWith - 1] == letter and word[endWith - 1] != letter:
            validLines += 1
        elif word[startWith - 1] != letter and word[endWith - 1] == letter:
            validLines += 1

    print (F'part 2 validLines = {validLines}')


def main():
    part_2()

    
if __name__ == "__main__":
    main()
