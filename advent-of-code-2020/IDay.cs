﻿namespace AdventOfCoded2020
{
    public interface IDay
    {
        int Day { get; }
        void Execute();
    }
}
