# TASK https://adventofcode.com/2020/day/6

from typing import List
from functools import reduce
from operator import __and__

def get_lines_of_group_from_file():
    with open('input.txt', 'r') as file:
        return file.read().split('\n\n')


class Group:
    def __init__(self, answers_of_person):
        self.answers_of_person = answers_of_person

    def get_uniq_answers(self) -> int:
        all_answers = []

        for value in self.answers_of_person.values():
            for answer in value:
                all_answers.append(answer)

        return len(set(all_answers))


    def get_length_set_of_and(self) -> int:
        values = self.answers_of_person.values()
        return len(reduce(__and__, map(lambda x: set(x), values)))


def get_groups_from_file():
    for group_lines in get_lines_of_group_from_file():
        lines = group_lines.split('\n')
        answers = []
        answers_of_person = {}

        for i, line in enumerate(lines):
            for answer in line:
                answers.append(answer)

            answers_of_person[i] = answers
            answers = []

        yield Group(answers_of_person)


def print_any_answers(groups : List[Group]):
    """
    print part 1 RESULT
    """
    print (f'any answers: {sum(map(lambda x: x.get_uniq_answers(), groups))}')


def print_all_answers(groups : List[Group]):
    """
    print part 2 RESULT
    """
    print (f'all answers: {sum(map(lambda x: x.get_length_set_of_and(), groups))}')


def main():
    groups = list(get_groups_from_file())

    print_any_answers(groups)
    print_all_answers(groups)


if __name__ == "__main__":
    main()