﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCoded2020.Day07
{
    internal class Day7 : DayBase
    {
        private const string KEY_CONTAIN = "contain";

        public override int Day => 7;

        private IEnumerable<Bag> GetBagEnumerable()
        {
            foreach (string line in GetInput())
            {
                string[] twoWords = line.Split(KEY_CONTAIN, StringSplitOptions.RemoveEmptyEntries);
                int lastIndexSpace = twoWords[0].TrimEnd().LastIndexOf(' ');
                string rootColorBag = twoWords[0].Substring(0, lastIndexSpace);

                var nestedBags = twoWords[1].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(colorOfBagString =>
                {
                    string[] words = colorOfBagString.Split(' ', StringSplitOptions.RemoveEmptyEntries);

                    return (words[0]) switch
                    {
                        string word when int.TryParse(word, out int count) => new Bag(int.Parse(words[0]), $"{words[1]} {words[2]}"),
                        "no" => null,
                        _ => throw new Exception(),
                    };
                }).Where(b => b != null).ToList();

                yield return new Bag(1, rootColorBag, nestedBags);
            }
        }

        public override void Execute()
        {
            var bags = GetBagEnumerable().ToDictionary(b => b.ColorName, b => b.NestedBags);

            int count = 0;
            GoldBagSearcher searcher = new GoldBagSearcher(bags);

            foreach (string bagColorName in bags.Keys)
            {
                if (searcher.IsGoldBag(bagColorName))
                {
                    count++;
                }
            }

            Console.WriteLine($"part 1 count : {count}");

            int totalCount = searcher.GetGoldenBagsInside();
            Console.WriteLine($"part 2 count : {totalCount}");
        }
    }
}
