﻿using System.Collections.Generic;

namespace AdventOfCoded2020.Day07
{
    class Bag
    {
        public int Count { get; }

        public string ColorName { get; }

        public List<Bag> NestedBags { get; }

        public Bag(int count, string colorName, List<Bag> nestedBags = null)
        {
            Count = count;
            ColorName = colorName;

            NestedBags = nestedBags is null ? new List<Bag>() : new List<Bag>(nestedBags);
        }

        public override string ToString() => $"{ColorName} ({Count}) NestedBags={NestedBags.Count}";
    }
}
