﻿using System.Collections.Generic;

namespace AdventOfCoded2020.Day07
{
    internal class GoldBagSearcher
    {
        private readonly Dictionary<string, List<Bag>> _bags;
        private readonly Dictionary<string, bool> _cacheBags;
        private const string SHINY_GOLD = "shiny gold";

        public GoldBagSearcher(Dictionary<string, List<Bag>> bags)
        {
            _bags = bags;
            _cacheBags = new Dictionary<string, bool>();
        }

        internal bool IsGoldBag(string colorName)
        {
            if (_cacheBags.TryGetValue(colorName, out bool result))
            {
                return result;
            }

            if (_bags.TryGetValue(colorName, out List<Bag> holdBags))
            {
                foreach (Bag bag in holdBags)
                {
                    if (bag.ColorName == SHINY_GOLD)
                    {
                        _cacheBags[colorName] = true;
                        return true;
                    }
                }

                foreach (Bag bag in holdBags)
                {
                    if (IsGoldBag(bag.ColorName))
                    {
                        _cacheBags[colorName] = true;
                        return true;
                    }
                }
            }

            _cacheBags[colorName] = false;
            return false;
        }

        private int GetCountBags(string colorName)
        {
            if (_bags.TryGetValue(colorName, out List<Bag> holdBags))
            {
                int count = 1;

                foreach (var bag in holdBags)
                {
                    count += bag.Count * GetCountBags(bag.ColorName);
                }

                return count;
            }

            return 0;
        }

        internal int GetGoldenBagsInside() => GetCountBags(SHINY_GOLD) - 1;
    }
}
