from typing import List

def get_lines_from_file():
    with open('input.txt', 'r') as file:
        return file.read().splitlines()

class Seat:
    def __init__(self, row: int, column : int):
        self.ROW = row
        self.COLUMN = column
    
    def get_Id(self) -> int:
        return self.ROW * 8 + self.COLUMN


def get_seats(lines):
    for line in lines:
        rows_range = [i for i in range(128)]
        columns_range = [i for i in range(8)]

        rows_letters = line[:7]
        columns_letters = line[7:]

        for symbol in rows_letters:
            index_row_range = int(len(rows_range) / 2)

            if symbol == 'F':
                rows_range = rows_range[:index_row_range]
            elif symbol == 'B':
                rows_range = rows_range[index_row_range:]

        for symbol in columns_letters:
            index_columns_range = int(len(columns_range) / 2)

            if symbol == 'L':
                columns_range = columns_range[:index_columns_range]
            elif symbol == 'R':
                columns_range = columns_range[index_columns_range:]

        yield Seat(row=rows_range[0], column=columns_range[0])


def main():
    lines = get_lines_from_file()
    seats = get_seats(lines)
    id_seats: List[int] = list(map(lambda x : x.get_Id(), seats))
    # id_seats: List[int] = seats.Select(s => s.get_Id()).ToList()

    for id in range(min(id_seats), max(id_seats)):
        if id not in id_seats:
            print (f'my id = {id}')

    print(f"Max id: {max(id_seats)}")


if __name__ == "__main__":
    main()
